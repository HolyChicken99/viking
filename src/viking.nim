# files are updated every 2 seconds ?
# it is to also count time 0 ?



import std/json
import yahttp
import dotenv
import os
import csv
import std/times
import std/options
import strutils

load("/home/lpha/Documents/projects/viking/src", ".env")

const HISTORICAL_ENDPOINT: string = "https://financialmodelingprep.com/api/v3/historical-chart/1min"
const REALTIME_ENDPOINT: string = "https://financialmodelingprep.com/api/v3/quote"

type
  HistoricalData = object
    date: string
    open: float
    low: float
    high: float
    close: float
    volume: int

type
  StockData = object
    symbol: string
    name: string
    price: float
    changesPercentage: float
    change: float
    dayLow: float
    dayHigh: float
    yearHigh: float
    yearLow: float
    marketCap: int
    priceAvg50: float
    priceAvg200: float
    exchange: string
    volume: int
    avgVolume: int
    open: float
    previousClose: float
    eps: float
    pe: float
    earningsAnnouncement: string
    sharesOutstanding: int
    timestamp: int


type
  RealTimeData = object
    price: float
    timestamp: int


proc isTimeEven(str: string): bool =
  let time = (str.split(" "))[1]
  let minutes = time.split(":")[1]
  if minutes.parseInt() mod 2 == 0:
    return true
  return false

proc inThePast3Days(str: string): bool =
  var d1 = getTime()
  let d2 = (d1 - 1.days).format("yyyy-MM-dd")
  let d3 = (d1 - 2.days).format("yyyy-MM-dd")
  let d4 = (d1).format("yyyy-MM-dd")

  var dates = @[d4, d2, d3]
  if str in dates:
    return true
  return false


# get the historical data of a stock from 3 days ago to now
# the data is in 1 minute intervals
# but convert it to 2 minute intervals
proc historicalFunction(stock: string) =
  var historicalDataArray: seq[HistoricalData]
  var historicalDataArray2: seq[HistoricalData]
  var startTime = (getTime() - 3.days).format("yyyy-MM-dd")
  var endTime = getTime().format("yyyy-MM-dd")
  let apiKey = os.getenv("API")
  let url = HISTORICAL_ENDPOINT & "/" & stock & "?" & "from=" & startTime &
      "to=" & endTime & "&apikey=" & apiKey

  let response = parseJson((yahttp.get(url)).body)
  for i in response:
    let entry = i.to(HistoricalData)
    if isTimeEven(entry.date):
      historicalDataArray.add(entry)

    var iter_nim = 0

    for i in historicalDataArray:
      #first entry
      if iter_nim == 0:
        historicalDataArray2.add(i)
        iter_nim += 1
      else:
        var temp = HistoricalData(date: i.date, open: historicalDataArray[
            iter_nim-1].close, low: i.low,
                                  high: i.high,
                                      close: i.close,
            volume: i.volume)
        historicalDataArray2.add(temp)
        iter_nim += 1

  # write to csv in the format open, close, time
  var file = open("historicalDatanew.csv", fmAppend)
  file.write("OPEN_PRICE,CLOSE_PRICE,TIME\n")


  for i in historicalDataArray2:
    file.write($i.open & "," & $i.close & "," & $i.date & "\n")
    echo $i.open & "," & $i.close & "," & $i.date


  flushFile(file)
  file.close()




proc realTimeFunction(stock: string) =
  let apiKey = os.getenv("API")
  while true:
    let url = REALTIME_ENDPOINT & "/" & stock & "?" & "apikey=" & apiKey
    let response = parseJson((yahttp.get(url)).body)
    for i in response:
      let entry = i.to(StockData)
      var temp = RealTimeData(price: entry.price, timestamp: entry.timestamp)
      # echo $temp.price & "," & $temp.timestamp
    sleep(300)

proc historicFunction(file: string) =
  var file = open(file, fmRead)
  var csv = file.readAll()
  var csv_lines = csv.split("\n")
  file.close()
  var iter = 0
  var file1 = open("EndofDay.csv", fmAppend)
  for i in csv_lines:
    if iter == 0:
      iter += 1
    else:
      # echo type(i)
      let line = i.split(",")
      let date = line[2].split(" ")[0]
      if inThePast3Days(date):
        file1.write(i & "\n")


      iter += 1










historicFunction("historicalDatanew.csv")
# realTimeFunction("AAPL")
# discard inThePast3Days("AAPL")
# historicalFunction("AAPL")
