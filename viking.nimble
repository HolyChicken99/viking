# Package

version       = "0.1.0"
author        = "holychicken99"
description   = "Helm the stock market"
license       = "MIT"
srcDir        = "src"
bin           = @["viking"]


# Dependencies

requires "nim >= 2.0.2"
requires "yahttp"
requires "csv"
requires "dotenv"
